#!/bin/bash
ORIGINAL_DATA_PATH=data/original_data
AUGMENT_DATA_PATH=data/augment_data


if [ ! -d $ORIGINAL_DATA_PATH ]; then
  echo "$ORIGINAL_DATA_PATH does not exist, create one" 
  mkdir -p $ORIGINAL_DATA_PATH;
fi

echo "Download original data"
gdown https://drive.google.com/u/0/uc?id=1QNf7x8EUGGA1zmzABqj3Qx1J1XpTNBQw&export=download
tar -xvf original_data.tar.xz
mv original_data data/
rm original_data.tar.xz

if [ ! -d $AUGMENT_DATA_PATH ]; then
  echo "$AUGMENT_DATA_PATH does not exist, create one" 
  mkdir -p $AUGMENT_DATA_PATH;
fi

echo "Download augmented data"
gdown https://drive.google.com/file/d/1QNf7x8EUGGA1zmzABqj3Qx1J1XpTNBQw/view?usp=sharing
mv augment_nmt_data.csv data/augment_data/
