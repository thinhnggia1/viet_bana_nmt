#!/bin/bash

TRAIN_FILE=./dataset/train.bpe
VALID_FILE=./dataset/valid.bpe
LANGUAGE=enko
GPU_DEVICE=0
BATCH_SIZE=128
N_EPOCHS=30
MAX_LENGTH=100
DROPOUT=0.2
HIDDEN_SIZE=768
N_LAYERS=4
MAX_GRAD_NORM=1e+8
ITERATION_PER_UPDATE=32
LR=1e-3
LR_STEP=0
MODEL_FILE=./runs/model.pth

PYTHONPATH=./ python tools/train_simple_nmt.py --train TRAIN_FILE --valid VALID_FILE --lang LANGUAGE \
--gpu_id GPU_DEVICE --batch_size BATCH_SIZE --n_epochs N_EPOCHS --max_length MAX_LENGTH --dropout DROPOUT \
--hidden_size HIDDEN_SIZE --n_layers N_LAYERS --max_grad_norm MAX_GRAD_NORM --iteration_per_update ITERATION_PER_UPDATE \
--lr LR --lr_step LR_STEP --use_adam --use_transformer --rl_n_epochs 0 \
--model_fn MODEL_FILE
