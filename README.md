# Viet_Bana_NMT

`Description`: This is a neural machine translation project for translating from Vietnamese to Bana language.

### Project directory tree
```
.
├── data
│   ├── original_data
│   ├── augment_data
├── scripts
├── docs
├── tools
├── src                   
│   ├── simple_nmt
├── requirements.txt
└── README.md
```

### Dataset
The dataset contains two types of data, original_data and augment_data. 
- The original data is obtained from this original repos: https://github.com/nsphuoc10/transformer-based. 
- The augmented data is generated with backtranslation from MTrans (MTrans can be found here https://drive.google.com/drive/folders/1-_EII7La-4ULPrUvCT7G05MTfOd_bnki).

### Installation
If using pip run the following commads:
```
python -m venv nmt_env
source nmt_env/bin/activate
pip install -r requirements
```

If using anaconda run the following commands:
```
conda create --name nmt_env
conda activate nmt_env
pip install -r requirements
```

### Prepare data
To download different datasets. run the following command
```
bash scripts/download_data.sh
```

### Applications
TODO: Add different application scripts
